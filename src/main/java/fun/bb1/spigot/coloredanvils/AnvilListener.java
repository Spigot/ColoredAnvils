package fun.bb1.spigot.coloredanvils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import fun.bb1.spigot.coloredanvils.config.Config;
import net.md_5.bungee.api.ChatColor;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class AnvilListener implements Listener {
	
	private @NotNull final JavaPlugin plugin;
	private @NotNull final Config config;
	
	@Internal
	AnvilListener(@NotNull final JavaPlugin plugin, final @NotNull Config config) {
		this.plugin = plugin;
		this.config = config;
		Bukkit.getPluginManager().registerEvents(this, this.plugin);
	}
	
	@EventHandler
	public final void onAnvil(@NotNull final PrepareAnvilEvent event) {
		if (!event.getView().getPlayer().hasPermission("anvil.color")) return;
		final AnvilInventory inv = event.getInventory();
		if (inv.getItem(0) == null || inv.getItem(0).getType() == Material.AIR) return;
		if (inv.getItem(1) == null || inv.getItem(1).getType() == Material.AIR) return;
		final String color = this.config.getColorFor(inv.getItem(1));
		if (color == null) return;
		final ItemStack itemStack = inv.getItem(0).clone();
		final ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setDisplayName(color + ChatColor.stripColor(itemMeta.getDisplayName()));
		itemStack.setItemMeta(itemMeta);
		SetAnvilRunnable.run(this.plugin, inv, itemStack);
	}
	
	private final @NotNull ItemStack convert(@NotNull final ItemStack itemStack, @NotNull final String name, @Nullable final ItemStack secondary) {
		if (secondary == null || secondary.getType() == Material.AIR) return itemStack;
		final String color = this.config.getColorFor(secondary);
		if (color == null) return itemStack;
		final ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.setDisplayName(color + name);
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}
	
}
