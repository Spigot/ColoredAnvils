package fun.bb1.spigot.coloredanvils;

import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class SetAnvilRunnable extends BukkitRunnable {
	
	@Internal
	static final void run(@NotNull final JavaPlugin plugin, @NotNull final AnvilInventory inv, @NotNull final ItemStack newItemStack) {
		new SetAnvilRunnable(inv, newItemStack).runTaskLater(plugin, 0);
	}
	
	private @NotNull final AnvilInventory inv;
	private @NotNull final ItemStack newItemStack;
	
	@Internal
	SetAnvilRunnable(@NotNull final AnvilInventory inv, @NotNull final ItemStack newItemStack) {
		this.inv = inv;
		this.newItemStack = newItemStack;
	}
	
	@Override
	public final void run() {
		this.inv.setItem(2, this.newItemStack);
		this.inv.setRepairCostAmount(1);
		this.inv.setRepairCost(1);
	}
	
}
