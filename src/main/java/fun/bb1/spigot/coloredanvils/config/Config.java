package fun.bb1.spigot.coloredanvils.config;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.md_5.bungee.api.ChatColor;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
public final class Config {
	
	private @NotNull final JavaPlugin plugin;
	
	@Internal
	public Config(@NotNull final JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	public final @Nullable String getColorFor(@NotNull final ItemStack itemStack) {
		final String key = itemStack.getType().name();
		final String hexStr = this.plugin.getConfig().getString(key);
		if (hexStr == null) return null;
		if (hexStr.length() != 6) {
			this.plugin.getLogger().warning("Invalid hex code provided for key: " + key);
			return null;
		}
		return ChatColor.of("#" + hexStr).toString();
	}
	
}
