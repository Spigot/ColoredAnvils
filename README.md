# Colored Anvils

A Minecraft plugin for giving items name's color in an anvil

> *Reload ready! This plugin has no adverse effects when /reload is executed: commands will be disabled and added according to the changes to the configuration*

## Configuration

All used items and their colors can be configured:

It is a simple [Material](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html) -> HexColor map

```yml
WHITE_DYE: FFFFFF
ORANGE_DYE: FFAA00
MAGENTA_DYE: FF00FF
LIGHT_BLUE_DYE: 5555FF
YELLOW_DYE: FFFF55
LIME_DYE: 55FF55
PINK_DYE: FF55FF
GRAY_DYE: "555555"
LIGHT_GRAY_DYE: AAAAAA
CYAN_DYE: 55FFFF
PURPLE_DYE: AA00AA
BLUE_DYE: 0000AA
BROWN_DYE: "834333"
GREEN_DYE: 00AA00
RED_DYE: FF5555
BLACK_DYE: "000000"
```

To add new items simply append the config.yml file with the material name and the hex color to use

## Permissions

The permission `anvil.color` is checked before changing the color, this is provided to all players by default.

## Images

![Colored Item](https://i.ibb.co/pRVZ2k4/image.png)

![Renamed Item](https://i.ibb.co/N2JPM4K/image.png)
